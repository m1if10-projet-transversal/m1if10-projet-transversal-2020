
# Pointeurs vers des ressources utiles pour l'UE

## Gestion de projet

### Travail à distance
- [Le guide de démarrage du travail à distance de GitLab](https://about.gitlab.com/company/culture/all-remote/remote-work-starter-guide/)
- [Le guide du 100% distantiel](https://about.gitlab.com/company/culture/all-remote/guide/)

### Documenter son travail 
- Introduction à l'utilisation du wiki: [pourquoi l'entreprise GitLab s'organise autour d'un manuel partagé géant](https://about.gitlab.com/handbook/handbook-usage/#why-handbook-first)

