# M1IF10 Projet Transversal 2020-2021

Cette UE vise à développer les capacités de travail collaboratives, tout en mettant en pratique les connaissance acquises dans les autres UE du M1.

Encadrants: Emmanuel Coquery, Lionel Médini, Matthieu Moy, Aurélien Tabard (responsable UE).

## Organisation de l'UE

Il s'agira de mener un projet de développement Web en groupe de 6 personnes. Le temps de travail attendu par étudiant est de 90h réparties au fil des semaines.

L'UE se déroule pendant les périodes "creuses". Nous avons bloqué des créneaux, mais vous êtes libre dans votre organisation.
L'UE se déroulera d'abord en ligne, en fonction des conditions sanitaires, vous pourrez (ou non) vous rencontrer pour travailler en groupe. Mais tout devra être fait pour que des personnes à distance puissent participer de manière fluide au projet.

Si vous avez des questions techniques ou organisationelles, elles se discuteront de manière publique via des ["issues" gitlab](https://forge.univ-lyon1.fr/m1if10-projet-transversal/m1if10-projet-transversal-2020/-/boards) pour que tous les groupes puissent bénéficier des réponses. 

Nous offrirons une séance de permanence en présentiel ou à distance une fois par semaine (voir le calendrier), idéalement pour un groupe tout entier. Cela peut être l'occasion de discuter d'organisation, du cadrage du projet, de l'architecture, ou de problèmes techniques spécifiques. 

Nous n'utiliserons pas l'email ou le chat pour répondre aux questions techniques. Vous pouvez toutefois m'envoyer un email :
- si une réponse tarde à arriver dans les issues (pas de réponses après 36h).
- si vous souhaitez discuter de l'organisation de votre groupe ou de points plus personnels (difficulté à suivre, tensions dans le groupe, attitude de personnes dans le groupe).

## Permanences 
Pendant les semaines de projet il y aura une à deux permanences par semaine. Voir le [calendrier de l'UE (google cal).](https://calendar.google.com/calendar/embed?src=4cq51ctpn99qipor3h1o3uije8%40group.calendar.google.com&ctz=Europe%2FBerlin). 

La permanence se fera en présentiel ou en distanciel selon la situation. Le lien pour la visio en distanciel sera toujours le même : [https://jitsi-info.univ-lyon1.fr/mif10-permanence](https://jitsi-info.univ-lyon1.fr/mif10-permanence).


## Cours 

- Intro à l'UE ([supports pdf](https://forge.univ-lyon1.fr/m1if10-projet-transversal/m1if10-projet-transversal-2020/-/blob/master/cours/Projet_Transversal-2020.pdf))
- Intro à [Gitlab pour la gestion de projet (vidéo 15min)](https://www.youtube.com/watch?v=Dfc6c7TmmA0)
- [Gestion de projet à distance (vidéo 35min)](https://youtu.be/x9fDOf5KFAg)


## Projet

Le thème général pour cette année est la création d'une application permettant de gérer des évènements participatifs. 
On pense ici aux évènements organisés par une bibliothèque, une université, une association (type AML), un club sportif amateur, etc.

Voici des thèmes possibles, à spécifier au sein de votre groupe d'ici la fin de la 1e semaine. Vous pouvez proposer un sujet qui rentre dans ce cadre :

  - Un outil de gestion d'évènements (type meetup)
    - Création, 
    - Modification
    - Gestion de la participation de l'intérêt (like), à l'enregistrement (signup).
  - Un dispositif d'affichage public (comme les écrans publics à l'entrée du Nautibus)
    - Création et modification des évènements
    - Gestion de leur affichage sur des dispositifs dédidés.
  - Création automatisée/paramétrique de posters (ou de flyers) papiers, basés sur les données d'évènements.
  - Outil de gestion des évènements en eux mêmes 
    - gestion des inscriptions
    - suivi des participants (si entrées limitées)
  - Outil de documentation des évènements
    - permettre la prise de notes/photos lors d'évènements
    - permettre la publication de ces notes sur un espace public partagé
  - Outil spécifique aux évènements en ligne 
    - accès à une visio/stream
    - possibilité de faire des commentaires lives
  - ...



## Étapes de l'UE, jalons et rendus intermédiaires
Les rendus se font sur le wiki de projet gitlab

### Semaine 39 (23/09) - Lancement du projet  (5h de travail attendu)
- Création des groupes
- Mise en place des outils de collaboration
- Choix du sujet
- Jalon 0 :
  - Groupe Gitlab créé
  - Projet Gitlab créé
  - Wiki: 
    - Page de garde créée qui organisera le contenu au fur et à mesure du projet
    - Page de équipe créée avec la liste des membres du projet (Nom, prénom, email, _téléphone_) + lien vers le groupe RocketChat + lien votre salle de visio préféré qui restera le même (ex: jitsi-info.univ-lyon1.fr/mif10-gpe132)
  - Enseignants de l'UE rajoutés comme rapporteurs au projet Gitlab.

### Semaine 40 (28/09) - Organisation et conception        (12h de travail attendu)
- Organisation du travail de groupe
- Phase de conception orientée usages
- Jalon 1 : 
  - Modalité de travail en groupe (page équipe du wiki mise à jour en fonction)
    - Rôles dans l’équipe, 
    - Organisation interne définie
  - Objectifs et cible utilisateur définie (2 ou 3 paragraphe sur la page d'accueil du wiki)
  - Partie UX sur le wiki:
    - Cas d’utilisations principaux de l'application (ensemble de user story)
    - Maquettes écrans de l'application (restez simple, il faudra le coder plus tard)
  - Des issues décrivant les users story (si ce n'est pas sur le wiki).

### Semaine 43 (19/10) - Stack et démarrage     (15h de travail attendu)
- Mise en place de la VM 
- Démarrage technique 
- Intégration continue mise en place
- Jalon 2 : 
  - Hello-world permettant d'afficher une entrée (simple) d'une base de donnée.
  - Maj du wiki avec pointeurs techniques 
    - How-to intégration continue pour tous les membres de l'équipe.
    - Outils et frameworks/bibliothèques utilisées par le groupe
    - 1e jet d’architecture (qui sera mis à jour au fur et à mesure)


### Semaine 44 (26/10) - Proto 1 “[MVP](https://en.wikipedia.org/wiki/Minimum_viable_product)”  (20h de travail attendu)
- Une version 0 de l'application est testable sur la VM
- Jalon 3 : 
  - Chaîne d’outillage fonctionnelle (Intégration continue : construction, tests, qualité)
  - UNE fonctionnalité de base est implémentée et intégrée. 


### Semaine 47 (16/11) (20h de travail attendu)
- Développement et tests

### Semaine 48 (23/11) — Livraison et présentation (15h de travail attendu)
- Finalisation (pas d’ajout de fonctionnalités) 
- Livraison du code
- Démo sur VM
- Soutenances
- Jalon 4 : 
  - Guide d'utilisation sur le wiki
  - MaJ de l'architecture
  - Documentation technique



Rendus et soutenances voir plus bas.



## Rôle au sein du groupe

Nous suggérons de définir les roles suivants dans le groupe
  - Spécialiste VM et de l'intégration 
  - Spécialiste back-end 
  - Spécialiste front
  - Spécialiste qualité et testing
  - Spécialiste produit (choix de design, cohérence de l'application) / UI
  - Spécialiste suivi de la progression, reporting, cohérence de l'équipe.

Ses rôles ne sont pas exclusifs (on peut en partager les rôles à deux personnes par exemple en se répartissant les tâches).


## Stand-up et travail à distance 
L'UE est pensée pour du distanciel d'abord. Cela ne vous empêchera pas de vous retrouver, mais il faut qu'une personne qui serait mise en quarantaine, ou qui ne puisse pas rencontrer les autres membres du groupe puisse suivre de manière fluide. Cela veut dire que toute information échangée entre n'importe quel membre de l'équipe soit visible par les autres, et stockée de manière pérenne. 

En termes pratiques nous attendons de chaque groupe :
  - 3 stand-ups dans la semaine à heure et lieu fixe (à minima). 
      - Le stand-up du début de semaine doit contenir une activité de planification des tâches de la semaine via des issues gitlab. 
      - Un stand-up de milieu de semaine qui peut être rapide.
      - Le stand-up de fin de semaine suivi d'un retro, qui fait le bilan des tâches réalisées, de celles qui ne l'ont pas été et du pourquoi.
  - une définition de toutes les tâches à réaliser dans une issue gitlab (en mode 'Board' pour leur suivi), cela inclut les tâches non-techniques (rapports, gestion du serveur, UI, etc.), les tâches doivent être mises à jour au fur et à mesure de la progression en suivant une approche kanban.
  - un commit (avec description) pour chaque tâche réalisée.

### Rendus chaque semaine
À la fin de chaque mêlée (1 semaine) nous attendons un bilan sur une page dédiée du wiki avec:
  - Liens vers les notes des réunions
  - Présents/absents aux 3 stand-ups de la semaine
  - Résumé de l'avancement
    - Points de blocage technique et/ou humains
    - États des issues en début et fin de semaine (screenshots de l'état du board en début et fin de semaine)
    - État de la qualité
    - Screenshots Sonarqube (à partir de la semaine 26).



## Outils à utiliser:
  - Gitlab pour le code, mais aussi pour la gestion des tâches, et pour les rendus des jalons.
  - Gitlab issues pour les questions techniques ouvertes à tous les groupes, votre groupe utilisera les issues de son projet pour les discussions internes.
  - Le wiki de Gitlab pour gérer l'information relative au projet.
  - RocketChat pour les échanges de coordinations ou les questions pratiques. Nous décourageons fortement Discord ou Messenger que vous utilisez déjà pour d'autres activités (fermez les quand vous travaillez!!!).
  - Un outil de visio (type jitsi) pour les réunions d'équipe à distance, ou même des sessions de codage collaboratives.


## Évaluation

Le projet sera évalué en trois pans:

  - Rendus intermédiaires
  - [Présentation et démo](eval-enseignants.md)
  - [Évaluation entre pairs](eval-pairs.md)

L'évaluation par les pairs aura un effet modulateur +1/0/-1 sur la note de groupe. L'équipe enseignante aura aussi la possibilité de rajouter un bonus/malus entre +2 et -2. Les notes au sein d'un groupe peuvent donc varier de -3 à +3. 



## Rendu Final
  
- Le rendu du code est pour le mardi **24/11 23h59**
- Le wiki peut être mis à jour jusqu'au jeudi **26/11 12h00**
- L'évaluation entre pair est à réaliser avant le **27/11 23h59**

- Mettre les intervenants de l'UE comme Reporter de votre projet
- Créer une branche FINAL correspondant au code de la démo
  - Qui datera au plus tard du jour du rendu le **26/11**
- README.md détaillant les dépendances, la procédure de build, et le lien vers une VM de démp
- Un wiki structuré qui contient les rendus intermédiaires et les notes de réunions.
- Le wiki décrira aussi le processus de gestion qualité
  - Tests, et leur gestion (qui écrit, qui teste, etc.)
  - Description des processus automatisés (mis en place ou essayé)
  - Rapports des tests utilisateurs
  - Captures intermédiaires de la qualité (avec Sonarqube)
- Le wiki aura une page dédié à la démo : 
  - Lien vers la VM de démo
  - Instructions d'utilisations
  

## Démonstration et Présentation 

### Déroulé des soutenances 

Les soutenances durent 25 minutes elles sont divisées en 
- 15 minutes de présentation et démo
- 10 minutes de questions

La durée de présentation est stricte (15 minutes), vous serez interrompus si vous dépassez.

Nous vous invitons à partager la parole de manière équilibrée au sein du groupe. 

### Soutenances en ligne
Les soutenances se dérouleront en ligne sur [Big Blue Button](classe-info.univ-lyon1.fr/). [Ici une salle ouverte qui devrait vous permettre de tester](https://classe-info.univ-lyon1.fr/tab-snc-8v7-yx3).

Chaque groupe aura une salle de soutenance dédiée pour pouvoir "s'installer". La salle sera allouée à chaque groupe pour que vous puissiez y faire vos répétitions (voir ci-dessous). 

Une personne du groupe sera chargée de gérer la présentation via un partage d'écran ou un partage de fichier sur BBB. Une autre personne chargée de la démo via un partage d'écran. 

Pour la soutenance, nous attendons à ce que tous les membres du groupe aient leur vidéo activée, quitte à la couper en cas de problème de bande passante. 

Si pour une raison ou une autre vous anticipez des complications, merci de prendre contact au plus tôt avec Aurélien Tabard. Pour que nous puissions trouver une alternative.


### Conseils de présentation

L'objectif de la présentation est de montrer votre réalisation (sous forme de démo), vos choix techniques, et votre organisation de groupe.

**Démonstration**
  - Préparer un scénario de démonstration, et déroulez le.
  - Mettre en avant les points forts de la réalisation aussi bien technique, qu'en terme d'usage.
  - Discuter des compromis que vous avez du faire, et de vos choix finaux en termes de fonctionalités, d'interface, de sécurité, etc.

**Présentation**
  - Présenter votre mode de fonctionnement collaboratif
  - Présenter vos choix d'architecture, quelques diagrammes UML, les patterns utilisés, etc.
  - Vos méthodes et outils de suivi et de déploiement du code
  - Présenter les résultats de votre [rétrospective Agile](https://www.nutcache.com/fr/blog/learning-matrix-iteration-agile/) (Les points positifs, Les points à améliorer, Les « merci ! », Les idées d’amélioration)
  - Quelles sont les leçons que vous retenez du projet ? 

Nous valorisons la prise de recul critique sur ce que vous avez appris, et comment vous vous organiserez mieux la prochaine fois que ce soit en termes techniques ou de travail de groupe. 


### Passage des groupes:

#### 26/11
Jury: Lionel Médini, Emmanuel Coquery
 - 11:15 - 11:45 : Groupe 8 - [salle BBB](https://classe-info.univ-lyon1.fr/coq-py4-cbg-rg1)
 - pause

 Jury: Aurélien Tabard, Emmanuel Coquery
 - 13:00 - 13:30 : Groupe 17 - [Google meet](https://meet.google.com/cgq-runk-bco)
 - 13:30 - 14:00 : Groupe 14 - [salle BBB](https://classe-info.univ-lyon1.fr/coq-h3s-re3-x5r)
 - 14:00 - 14:30 : Groupe 4 - [salle BBB](https://classe-info.univ-lyon1.fr/tab-lsn-ds5-0ij)
 - pause
 - 14:15 - 14:45 : Groupe 13 - [salle BBB](https://classe-info.univ-lyon1.fr/med-lvk-mjx-wbn)
 - 14:45 - 15:15 : Groupe 18 - [salle BBB](https://classe-info.univ-lyon1.fr/moy-5h8-ba9-3xg)
 - 15:15 - 15:45 : Groupe 10 - [salle BBB](https://classe-info.univ-lyon1.fr/moy-o13-hud-kfv)
 - pause
 - 16:00 - 16:30 : Groupe 5 - [salle BBB](https://classe-info.univ-lyon1.fr/tab-n1g-hbv-fek)


#### 27/11
Jury: Aurélien Tabard, Matthieu Moy
 - 8:00 - 8:30 : Groupe 3  - [salle BBB](https://classe-info.univ-lyon1.fr/tab-sy7-guu-xmk)
 - 8:30 - 9:00 : Groupe 1 - [salle BBB](https://classe-info.univ-lyon1.fr/tab-5u6-f1q-dif)
 - 9:00 - 9:30 : Groupe 15 - [salle BBB](https://classe-info.univ-lyon1.fr/moy-uvm-ooc-jsn)
 - pause

 Jury: Lionel Médini, Matthieu Moy
 - 10:15 - 10:45 : Groupe 9 - [salle BBB](https://classe-info.univ-lyon1.fr/moy-aib-42s-jzl)
 - 10:45 - 11:15 : Groupe 16 - [salle BBB](https://classe-info.univ-lyon1.fr/moy-8hi-svb-5cw)
 - 11:15 - 11:45 : Groupe 6 - [salle BBB](https://classe-info.univ-lyon1.fr/tab-hso-zof-xaz)
 - pause
 - 12:00 - 12:30 : Groupe 7 - [Salle WebEx](
    https://univ-lyon1.webex.com/univ-lyon1-en/j.php?MTID=mb6a410d250f688d89fed41aaaf659755) (code orga : 691033) ~~[salle BBB](https://classe-info.univ-lyon1.fr/tab-arj-ke4-thq)~~
 - 12:30 - 13:00 : Groupe 11 - [salle BBB](https://classe-info.univ-lyon1.fr/moy-dl7-fsk-8x4)

 Jury: Aurélien Tabard, Matthieu Moy
 - 13:00 - 13:30 : Groupe 2 - [salle BBB](https://classe-info.univ-lyon1.fr/tab-qki-yhz-m6t)

 Jury: Aurélien Tabard, Emmanuel Coquery
 - 13:30 - 14:00 : Groupe 12 - [salle BBB](https://classe-info.univ-lyon1.fr/coq-nm6-hes-swu)


